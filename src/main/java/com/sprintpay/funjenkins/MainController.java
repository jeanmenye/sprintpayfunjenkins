package com.sprintpay.funjenkins;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MainController {

	  	@RequestMapping(value="/")
		public @ResponseBody String welcomeToApp() {
			return "<br><br> ##############################################################<br><br><pre>                  Welcome to FunJenkins Mr. Landry ! <pre> <br><br> ################################################################### <br><br>";
		}
}

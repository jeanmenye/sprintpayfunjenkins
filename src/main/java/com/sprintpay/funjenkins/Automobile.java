package com.sprintpay.funjenkins;


public class Automobile {
	
	private String color;
	
	private int tires;
	
	private String name;
	
	public Automobile(String name, String color, int tires) {
		this.color = color;
		this.tires = tires;
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getTires() {
		return tires;
	}

	public void setTires(int tires) {
		this.tires = tires;
	}
	
	public String toString() {
		return "Hello Sprint-Pay, \n" + this.getName() + " is " + this.getColor() + " and has " + this.getTires() + " tires";
	}

}

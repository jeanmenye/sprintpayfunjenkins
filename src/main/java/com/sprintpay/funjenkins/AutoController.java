package com.sprintpay.funjenkins;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class AutoController {
	
	
    @RequestMapping(value="/cadi", method=RequestMethod.GET)
	public String get() {
		Automobile auto = new Automobile("This Cadillac", "black", 4);
		return auto.toString();
	}

}
